# knowledgejunkie-deps-all

## Description

A [Debian GNU/Linux][debian] super-metapackage to install knowledgejunkie's
other metapackages for the following:

- apt sources/pins
- dotfiles
- packaging
- preferred applications
- sysadmin


## Building the metapackage

Build the binary package using git-buildpackage:

    $ gbp buildpackage

This will generate the binary .deb in gbp's configured output directory.


## Installing the metapackage

We can install the package using gdebi, which will automatically install
any missing dependencies if they can be located:

    # gdebi knowledgejunkie-deps-all_<version>_all.deb


## License

Copyright: 2020, Nick Morrott <nickm@debian.org>
License: GPL-2+


## Thanks

I hope you find this metapackage useful. Feel free to follow me on [GitHub][github] and [Twitter][twitter].

[debian]: https://www.debian.org/
[github]: https://github.com/knowledgejunkie
[twitter]: https://twitter.com/nickmorrott
